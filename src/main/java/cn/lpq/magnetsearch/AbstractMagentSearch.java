package cn.lpq.magnetsearch;

import java.util.List;

import cn.lpq.pojo.MagentInfo;

public abstract class AbstractMagentSearch {

	public abstract List<MagentInfo> magentSearch(String keyword);
	
}
